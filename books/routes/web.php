<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// routes for the assignment for books and authors
// all routes gas get request
Route::get('/books','BookController@index');
Route::get('/books/{book}','BookController@show');
Route::get('/author','AuthorController@index');
Route::get('/author/{author}','AuthorController@show');