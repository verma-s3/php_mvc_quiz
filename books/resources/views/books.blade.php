<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>BooksList</title>
</head>
<body>
	<h1>{{$title}}</h1>
	<ul>
		@foreach($books as $book)
		<li> <a href="/books/{{$book->id}}" style="text-decoration:none;">{{$book->title}}</a></li>
		@endforeach
	</ul>
</body>
</html>