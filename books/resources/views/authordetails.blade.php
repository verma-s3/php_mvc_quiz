<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Books</title>
</head>
<body>
	<h1>{{$title}}</h1>
	<table>
		<tr>
			<td>Name</td>
			<td>Country</td>
		</tr>
		<tr>
			<td>{{$author->name}}</td>
			<td>{{$author->country}}</td>
		</tr>
	</table>
</body>
</html>