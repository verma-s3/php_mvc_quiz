<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Books</title>
</head>
<body>
	<h1>{{$title}}</h1>
	<h2>{{$book->title}}</h2>
	<table>
		<tr>
			<td>Pages</td>
			<td>Price</td>
			<td>Year</td>
			<td>Author</td>
		</tr>
		<tr>
			<td>{{$book->num_pages}}</td>
			<td>{{$book->price}}</td>
			<td>{{$book->year_published}}</td>
			<td><a href="/author/{{$book->author_id}}">{{$book->author->name}}</a></td>
		</tr>
	</table>
</body>
</html>