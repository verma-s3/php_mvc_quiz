<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>BooksList</title>
</head>
<body>
	<h1>{{$title}}</h1>
	<ul>
		@foreach($authors as $author)
		<li> <a href="/author/{{$author->id}}" style="text-decoration:none;">{{$author->name}}</a></li>
		@endforeach
	</ul>
</body>
</html>