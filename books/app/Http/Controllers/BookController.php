<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{
    public function index()
    {
    	$books = Book::all();
    	//dd($books);
    	$title = 'Booklist';
    	return view('books',compact('title','books'));
    }

    public function show(Book $book)
    {
    	$title = 'Books Details';
    	return view('booksdetails',compact('title','book'));
    }
}
