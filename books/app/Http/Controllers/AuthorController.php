<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
class AuthorController extends Controller
{
    public function index()
    {
    	$authors = Author::all();
    	//dd($books);
    	$title = 'Author list';
    	return view('author',compact('title','authors'));
    }

    public function show(Author $author)
    {
    	$title = 'Author Details';
    	//dd($author);
    	return view('authordetails',compact('title','author'));
    }
}
