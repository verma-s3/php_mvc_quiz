<?php

use Illuminate\Database\Seeder;
//use DB;
class seed_books_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
        	'title'=>'Black Echo',
        	'author_id'=>1,
        	'num_pages'=>200,
        	'price'=>12.25,
        	'year_published'=>2016
        ]);
        DB::table('books')->insert([
        	'title'=>'Black Ice',
        	'author_id'=>1,
        	'num_pages'=>455,
        	'price'=>80.23,
        	'year_published'=>2011
        ]);
        DB::table('books')->insert([
        	'title'=>'Caves of Steel',
        	'author_id'=>2,
        	'num_pages'=>526,
        	'price'=>15.23,
        	'year_published'=>2017
        ]);
        DB::table('books')->insert([
        	'title'=>'Foundation',
        	'author_id'=>2,
        	'num_pages'=>45,
        	'price'=>12.10,
        	'year_published'=>2018
        ]);
        DB::table('books')->insert([
        	'title'=>'The Shininng',
        	'author_id'=>3,
        	'num_pages'=>35,
        	'price'=>12.56,
        	'year_published'=>2015
        ]);
        DB::table('books')->insert([
        	'title'=>'IT',
        	'author_id'=>3,
        	'num_pages'=>34,
        	'price'=>23.21,
        	'year_published'=>2012
        ]);
        DB::table('books')->insert([
        	'title'=>'Underworld',
        	'author_id'=>4,
        	'num_pages'=>45,
        	'price'=>52.23,
        	'year_published'=>2014
        ]);
        DB::table('books')->insert([
        	'title'=>'White Noise',
        	'author_id'=>4,
        	'num_pages'=>234,
        	'price'=>11.00,
        	'year_published'=>2011
        ]);
        DB::table('books')->insert([
        	'title'=>'Devil in a Blue Dress',
        	'author_id'=>5,
        	'num_pages'=>34,
        	'price'=>13.01,
        	'year_published'=>2013
        ]);
        DB::table('books')->insert([
        	'title'=>'Fear itself',
        	'author_id'=>5,
        	'num_pages'=>45,
        	'price'=>52.23,
        	'year_published'=>2016
        ]);
    }
}
