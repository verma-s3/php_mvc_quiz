<?php

use Illuminate\Database\Seeder;

class seed_authors_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
        	'name'=>'Micheal Countnelly',
        	'country'=>'Winnipeg'
        ]);
        DB::table('authors')->insert([
        	'name'=>'Isaac Asinnov',
        	'country'=>'Alberta'
        ]);
        DB::table('authors')->insert([
        	'name'=>'Stephen King',
        	'country'=>'British Columbia'
        ]);
        DB::table('authors')->insert([
        	'name'=>'Don Dehillo',
        	'country'=>'Nova Scotia'
        ]);
        DB::table('authors')->insert([
        	'name'=>'Walter Mosley',
        	'country'=>'Toronto'
        ]);
    }
}
